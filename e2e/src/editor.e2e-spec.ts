import {browser, logging, until, ExpectedConditions} from 'protractor';
import {EditorPage} from './editor.po';

describe('Editor Page', () => {
  let page: EditorPage;

  beforeEach(() => {
    page = new EditorPage();
  });

  it('toolbar should only have home icon and arrow', () => {
    page.navigateTo();
    expect(page.getToolbarText()).toEqual(['home\nkeyboard_arrow_right']);
  });

  it('upon clicking "add field" button, new field dialog should pop up', () => {
    page.navigateTo();
    page.addFieldButton().click().then(() => {
      expect(page.getAddFieldDialog()).toBeTruthy();
      expect(page.getAddFieldDialog().dialogElement).toBeTruthy();
    });
  });

  it('upon clicking "add collection" button, new collection dialog should pop up', () => {
    page.navigateTo();
    page.addCollectionButton().click().then(() => {
      expect(page.getAddCollectionDialog()).toBeTruthy();
      expect(page.getAddCollectionDialog().dialogElement).toBeTruthy();
      expect(page.getAddCollectionDialog().getDialogTitle()).toEqual(['new collection']);
    });
  });

  it('add a string field f1', () => {
    page.navigateTo();
    page.addFieldButton().click().then(() => {
      const dialog = page.getAddFieldDialog();
      dialog.getFieldNameInput().sendKeys('f1')
        .then(() => dialog.getFieldValueInput().sendKeys('value1')
          .then(() => {
            expect(dialog.getFieldNameInput().getAttribute('value')).toEqual('f1');
            expect(dialog.getFieldValueInput().getAttribute('value')).toEqual('value1');
            dialog.getOkButton().click().then(() => {
              expect(page.getFieldList()).toEqual(['f1 :\nvalue1\ndelete', 'field1 :\nabc\ndelete', 'field2 :\n123\ndelete']);
            });
          }));
    });
  });


  it('cancel adding field', () => {
    page.navigateTo();
    page.addFieldButton().click().then(() => {
      page.getAddFieldDialog().getCancelButton().click().then(() => {
        expect(page.getFieldList()).toEqual([]);
      });
    });
  });

  it('add collection coln', () => {
    page.navigateTo();
    page.addCollectionButton().click().then(() => {
      const dialog = page.getAddCollectionDialog();
      const collectionName = dialog.getCollectionNameInput();
      collectionName.sendKeys('coln')
        .then(() => {
          expect(collectionName.getAttribute('value')).toEqual('coln');
          dialog.getOkButton().click().then(() => {
            expect(page.getCollectionList()).toEqual([
              'edit\ncol1\ndelete',
              'edit\ncol2\ndelete',
              'edit\ncol3\ndelete',
              'edit\ncoln\ndelete',
            ]);
          });
        });
    });
  });

  it('cancel adding collection', () => {
    page.navigateTo();
    page.addCollectionButton().click().then(() => {
      page.getAddCollectionDialog().getCancelButton().click().then(() => {
        expect(page.getCollectionList()).toEqual([]);
      });
    });
  });

  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    } as logging.Entry));
  });
});
