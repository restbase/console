import {browser, by, element, ElementFinder} from 'protractor';

export class AddFieldDialog {

  constructor(public dialogElement: ElementFinder) {
  }

  getDialogTitle() {
    return element.all(by.css('#mat-dialog-title-0'))
      .map(el => el.getText()) as Promise<string[]>;
  }

  getFieldNameInput() {
    return element(by.css('#mat-input-0'));
  }

  getFieldValueInput() {
    return element(by.css('#mat-input-1'));
  }

  getOkButton() {
    return element(by.css('#mat-dialog-0 > app-field-edit-dialog > mat-dialog-actions > button:nth-child(2)'));
  }

  getCancelButton() {
    return element(by.css('#mat-dialog-0 > app-field-edit-dialog > mat-dialog-actions > button:nth-child(1)'));
  }

}

export class AddCollectionDialog {

  constructor(public dialogElement: ElementFinder) {
  }

  getDialogTitle() {
    return element.all(by.css('#mat-dialog-title-0'))
      .map(el => el.getText()) as Promise<string[]>;
  }

  getCollectionNameInput() {
    return element(by.css('#mat-input-0'));
  }

  getOkButton() {
    return element(by.css('#mat-dialog-0 > app-collection-edit-dialog > mat-dialog-actions > button:nth-child(2)'));
  }

  getCancelButton() {
    return element(by.css('#mat-dialog-0 > app-collection-edit-dialog > mat-dialog-actions > button:nth-child(1)'));
  }

}

export class EditorPage {
  navigateTo() {
    return browser.get(`${browser.baseUrl}/edit`) as Promise<any>;
  }

  getToolbarText() {
    return element.all(by.css('body > app-root > app-editor > div > app-toolbar > mat-toolbar'))
      .map(el => el.getText()) as Promise<string[]>;
  }

  addFieldButton() {
    return element(by.css('#btn-add-field'));
  }

  addCollectionButton() {
    return element(by.css('#btn-add-collection'));
  }

  getAddFieldDialog(): AddFieldDialog {
    return new AddFieldDialog(element(by.css('#mat-dialog-0')));
  }

  getAddCollectionDialog(): AddCollectionDialog {
    return new AddCollectionDialog(element(by.css('#mat-dialog-3')));
  }

  getFieldList() {
    return element.all(by.css('body > app-root > app-editor > div > app-document-editor > mat-card > mat-card-content > app-field-list > mat-action-list > button.mat-list-item.ng-star-inserted > div'))
      .map(el => el.getText()) as Promise<string[]>;
  }

  getCollectionList() {
    return element.all(by.css('body > app-root > app-editor > div > app-document-editor > mat-card > mat-card-content > app-collection-list > mat-action-list > button.mat-list-item.ng-star-inserted > div'))
      .map(el => el.getText()) as Promise<string[]>;
  }
}
