import { browser, by, element } from 'protractor';

export class AppPage {
  navigateTo() {
    return browser.get(browser.baseUrl) as Promise<any>;
  }

  getTitleText() {
    return element(by.css('body > app-root > app-header > mat-toolbar > span:nth-child(1)')).getText() as Promise<string>;
  }
}
