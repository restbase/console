import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { StoreModule } from '@ngrx/store';
import { reducers, metaReducers } from './store';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SharedModule } from './shared/shared.module';
import { HeaderComponent } from './header/header.component';
import { EditorModule } from './editor/editor.module';
import { FormsModule } from '@angular/forms';
import { CollectionEditDialogComponent } from './editor/collection-list/collection-edit-dialog.component';
import { FieldEditDialogComponent } from './editor/field-list/field-edit-dialog.component';
import { EffectsModule } from '@ngrx/effects';
import { ServerEffects } from './store/server.effects';
import { RestbaseStoreModule } from './store/restbase-store.module';
import { RestbaseEffects } from './store/restbase.effects';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    EffectsModule.forRoot([ServerEffects, RestbaseEffects]),
    BrowserAnimationsModule,
    FormsModule,
    SharedModule,
    EditorModule,
    RestbaseStoreModule,
  ],
  entryComponents: [
    CollectionEditDialogComponent,
    FieldEditDialogComponent,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
