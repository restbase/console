import { async, getTestBed, TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { routes as appRoutes } from './app-routing.module';
import { AppComponent } from './app.component';
import { CollectionEditorComponent } from './editor/collection-editor/collection-editor.component';
import { CollectionListComponent } from './editor/collection-list/collection-list.component';
import { DocumentEditorComponent } from './editor/document-editor/document-editor.component';
import { DocumentListComponent } from './editor/document-list/document-list.component';
import { routes as editorRoutes } from './editor/editor-routing.module';
import { EditorComponent } from './editor/editor.component';
import { FieldListComponent } from './editor/field-list/field-list.component';
import { ToolbarComponent } from './editor/toolbar/toolbar.component';
import { HeaderComponent } from './header/header.component';
import { SharedModule } from './shared/shared.module';
import { RestbaseStoreModule } from './store/restbase-store.module';

describe('AppRoutingModule', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule.withRoutes([...appRoutes, ...editorRoutes]),
        SharedModule,
        RestbaseStoreModule,
      ],
      declarations: [
        AppComponent,
        HeaderComponent,
        EditorComponent,
        ToolbarComponent,
        DocumentEditorComponent,
        CollectionEditorComponent,
        FieldListComponent,
        CollectionListComponent,
        DocumentListComponent,
      ],
    }).compileComponents();
  }));

  it('should redirect to /edit', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const injector = getTestBed();
    const router = injector.get(Router);
    fixture.detectChanges();

    return fixture.ngZone.run(() => router.navigate(['/']))
      .then(() => {
        expect(router.url).toEqual('/edit');
      });
  });

});
