import { Component, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { State } from '../store';
import { selectServerStatus } from '../store/server.selectors';
import { CheckStatus } from '../store/server.actions';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.sass']
})
export class HeaderComponent implements OnInit {

  status$ = this.store.pipe(select(selectServerStatus));

  constructor(
    private store: Store<State>
  ) { }

  ngOnInit() {
    this.store.dispatch(new CheckStatus());
  }

}
