import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderComponent } from './header.component';
import { SharedModule } from '../shared/shared.module';
import { StoreModule } from '@ngrx/store';
import { reducers, metaReducers } from '../store';
import { RestbaseStoreModule } from '../store/restbase-store.module';

describe('HeaderComponent', () => {
  let component: HeaderComponent;
  let fixture: ComponentFixture<HeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        SharedModule,
        RestbaseStoreModule,
      ],
      declarations: [HeaderComponent]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render title in toolbar', () => {
    const header = TestBed.createComponent(HeaderComponent);
    header.detectChanges();

    const compiled = header.debugElement.nativeElement;
    expect(compiled.querySelector('mat-toolbar > span:nth-child(1)').textContent).toContain('Restbase Console');
  });

});
