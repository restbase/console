import { async, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { CollectionEditorComponent } from './editor/collection-editor/collection-editor.component';
import { CollectionListComponent } from './editor/collection-list/collection-list.component';
import { DocumentEditorComponent } from './editor/document-editor/document-editor.component';
import { DocumentListComponent } from './editor/document-list/document-list.component';
import { EditorComponent } from './editor/editor.component';
import { FieldListComponent } from './editor/field-list/field-list.component';
import { ToolbarComponent } from './editor/toolbar/toolbar.component';
import { HeaderComponent } from './header/header.component';
import { SharedModule } from './shared/shared.module';
import { RestbaseStoreModule } from './store/restbase-store.module';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        SharedModule,
        RestbaseStoreModule,
      ],
      declarations: [
        AppComponent,
        HeaderComponent,
        EditorComponent,
        ToolbarComponent,
        DocumentEditorComponent,
        CollectionEditorComponent,
        FieldListComponent,
        CollectionListComponent,
        DocumentListComponent,
      ],
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

});
