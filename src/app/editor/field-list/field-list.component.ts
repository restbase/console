import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import { Document } from 'src/app/model/document';
import { Field } from 'src/app/model/field';
import { UpdateDocument } from 'src/app/store/restbase.actions';
import { State } from '../../store';
import { FieldDeleteDialogComponent } from './field-delete-dialog.component';
import { FieldEditDialogComponent } from './field-edit-dialog.component';

@Component({
  selector: 'app-field-list',
  templateUrl: './field-list.component.html',
  styleUrls: ['./field-list.component.sass']
})
export class FieldListComponent implements OnInit {

  @Input() document: Document;

  constructor(
    public dialog: MatDialog,
    private store: Store<State>,
  ) { }

  ngOnInit() {
  }

  onAddClick() {
    const dialogRef = this.dialog.open(FieldEditDialogComponent, {
      data: {
        field: new Field(''),
        isNew: true,
      }
    });
    dialogRef.afterClosed().subscribe((result: Field) => {
      // add document
      if (result && result.key && result.value) {
        const path = this.document.path();
        const doc = new Document().deserialize({ ...this.document });
        doc.set(result.key, result.value);
        this.store.dispatch(new UpdateDocument(path, new Document().deserialize(doc)));
      }
    });
  }

  onEditClick(field: Field) {
    const dialogRef = this.dialog.open(FieldEditDialogComponent, {
      data: {
        field: { ...field },
        isNew: false,
      }
    });
    dialogRef.afterClosed().subscribe((result: Field) => {
      // update document
      if (result && result.key && result.value) {
        const path = this.document.path();
        const doc = new Document().deserialize({ ...this.document });
        doc.delete(result.key);
        doc.set(result.key, result.value);
        this.store.dispatch(new UpdateDocument(path, new Document().deserialize(doc)));
      }
    });
  }

  onDeleteClick(event: Event, field: Field) {
    // prevent parent event happening (edit)
    event.preventDefault();
    event.stopImmediatePropagation();

    const dialogRef = this.dialog.open(FieldDeleteDialogComponent, {
      data: {
        field: { ...field },
      }
    });
    dialogRef.afterClosed().subscribe((result: Field) => {
      // update document
      if (result && result.key && result.value) {
        const path = this.document.path();
        const doc = new Document().deserialize({ ...this.document });
        doc.delete(result.key);
        this.store.dispatch(new UpdateDocument(path, new Document().deserialize(doc)));
      }
    });
  }
}
