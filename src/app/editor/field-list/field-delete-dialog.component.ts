import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Field } from 'src/app/model/field';

@Component({
  selector: 'app-field-delete-dialog',
  templateUrl: './field-delete-dialog.component.html',
})
export class FieldDeleteDialogComponent implements OnInit {

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: DialogInput,
    public dialogRef: MatDialogRef<FieldDeleteDialogComponent>,
  ) {
  }

  ngOnInit() {
  }

}

export class DialogInput {
  field: Field;
}
