import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Field } from 'src/app/model/field';

@Component({
  selector: 'app-field-edit-dialog',
  templateUrl: './field-edit-dialog.component.html',
  styleUrls: [
    './field-edit-dialog.component.sass',
    './field-list.component.sass',
  ]
})
export class FieldEditDialogComponent {

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: DialogInput,
    public dialogRef: MatDialogRef<FieldEditDialogComponent>,
  ) {
  }

  onCancelClick(): void {
    this.dialogRef.close();
  }
}

export class DialogInput {
  field: Field;
  isNew: boolean;
}
