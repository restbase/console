import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatDialogModule, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { FieldEditDialogComponent } from './field-edit-dialog.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { Field } from 'src/app/model/field';

describe('FieldEditDialogComponent', () => {
  let component: FieldEditDialogComponent;
  let fixture: ComponentFixture<FieldEditDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FieldEditDialogComponent ],
      imports: [
        SharedModule,
        MatDialogModule,
        FormsModule,
        BrowserAnimationsModule,
      ],
      providers: [
        { provide: MatDialogRef, useValue: {} },
        {
          provide: MAT_DIALOG_DATA, useValue: {
            field: new Field('test', 42),
            isNew: true,
          }
        },
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FieldEditDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have data', () => {
    expect(component.data).toBeTruthy();
    expect(component.data.isNew).toBeTruthy();
    expect(component.data.field).toBeTruthy();
    expect(component.data.field.key).toEqual('test');
    expect(component.data.field.value).toEqual(42);
  });

});
