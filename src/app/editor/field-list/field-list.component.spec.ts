import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Document } from 'src/app/model/document';
import { SharedModule } from 'src/app/shared/shared.module';
import { RestbaseStoreModule } from 'src/app/store/restbase-store.module';
import { FieldListComponent } from './field-list.component';


describe('FieldListComponent', () => {
  let component: FieldListComponent;
  let fixture: ComponentFixture<FieldListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FieldListComponent ],
      imports: [
        SharedModule,
        RestbaseStoreModule,
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FieldListComponent);
    component = fixture.componentInstance;
    component.document = Document.root();
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
