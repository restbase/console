import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FieldDeleteDialogComponent } from './field-delete-dialog.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { MatDialogModule, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Collection } from 'src/app/model/collection';

describe('FieldDeleteDialogComponent', () => {
  let component: FieldDeleteDialogComponent;
  let fixture: ComponentFixture<FieldDeleteDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FieldDeleteDialogComponent ],
      imports: [
        SharedModule,
        MatDialogModule,
        FormsModule,
        BrowserAnimationsModule,
      ],
      providers: [
        { provide: MatDialogRef, useValue: {} },
        {
          provide: MAT_DIALOG_DATA, useValue: {
            field: new Collection().deserialize({ name: 'test' }),
          }
        },
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FieldDeleteDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
