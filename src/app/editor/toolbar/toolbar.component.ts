import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Path, ROOT } from 'src/app/model/path';
import { Element } from '../../model/element';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.sass']
})
export class ToolbarComponent implements OnInit {

  @Input() element: Element;

  constructor(
    private router: Router,
  ) { }

  ngOnInit() {
  }

  navigateTo(path: Path) {
    const segments = ['/edit', ...path.segments];
    this.router.navigate(segments);
  }

  onRootClick() {
    return this.onPathClick(ROOT);
  }

  onPathClick(path: Path) {
    return this.navigateTo(path);
  }

}
