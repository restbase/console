import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MatIconModule } from '@angular/material/icon';
import { MatCardModule } from '@angular/material/card';
import { MatDividerModule } from '@angular/material/divider';
import { MatListModule } from '@angular/material/list';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatToolbarModule } from '@angular/material/toolbar';
import { CollectionListComponent } from './collection-list/collection-list.component';
import { CollectionEditDialogComponent } from './collection-list/collection-edit-dialog.component';
import { FieldEditDialogComponent } from './field-list/field-edit-dialog.component';
import { FieldListComponent } from './field-list/field-list.component';
import { DocumentEditorComponent } from './document-editor/document-editor.component';
import { EditorComponent } from './editor.component';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { EditorRoutingModule } from './editor-routing.module';
import { CollectionEditorComponent } from './collection-editor/collection-editor.component';
import { DocumentListComponent } from './document-list/document-list.component';
import { CollectionDeleteDialogComponent } from './collection-list/collection-delete-dialog.component';
import { FieldDeleteDialogComponent } from './field-list/field-delete-dialog.component';
import { DocumentDeleteDialogComponent } from './document-list/document-delete-dialog.component';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule } from '@angular/material/button';

@NgModule({
  declarations: [
    FieldListComponent,
    DocumentEditorComponent,
    CollectionListComponent,
    CollectionEditDialogComponent,
    FieldEditDialogComponent,
    EditorComponent,
    ToolbarComponent,
    CollectionEditorComponent,
    DocumentListComponent,
    CollectionDeleteDialogComponent,
    FieldDeleteDialogComponent,
    DocumentDeleteDialogComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    MatIconModule,
    MatCardModule,
    MatDividerModule,
    MatListModule,
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule,
    MatToolbarModule,
    BrowserModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatDialogModule,
    EditorRoutingModule,
  ],
  entryComponents: [
    CollectionEditDialogComponent,
    CollectionDeleteDialogComponent,
    FieldEditDialogComponent,
    FieldDeleteDialogComponent,
    DocumentDeleteDialogComponent,
  ],
  exports: [
    EditorComponent,
  ]
})
export class EditorModule { }
