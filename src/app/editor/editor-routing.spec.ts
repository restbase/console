import { async, ComponentFixture, getTestBed, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { Router, RouterModule, ActivatedRoute, ParamMap } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';
import { Document } from 'src/app/model/document';
import { SharedModule } from '../shared/shared.module';
import { CollectionEditDialogComponent } from './collection-list/collection-edit-dialog.component';
import { CollectionListComponent } from './collection-list/collection-list.component';
import { DocumentEditorComponent } from './document-editor/document-editor.component';
import { routes } from './editor-routing.module';
import { EditorComponent } from './editor.component';
import { FieldListComponent } from './field-list/field-list.component';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { switchMap } from 'rxjs/operators';
import { CollectionEditorComponent } from './collection-editor/collection-editor.component';
import { DocumentListComponent } from './document-list/document-list.component';
import { RestbaseStoreModule } from '../store/restbase-store.module';


describe('EditorRoutingModule', () => {
  let component: EditorComponent;
  let fixture: ComponentFixture<EditorComponent>;
  let router: Router;
  let route: ActivatedRoute;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        EditorComponent,
        ToolbarComponent,
        DocumentEditorComponent,
        CollectionEditorComponent,
        FieldListComponent,
        CollectionListComponent,
        CollectionEditDialogComponent,
        DocumentListComponent,
      ],
      imports: [
        RouterTestingModule.withRoutes([...routes]),
        SharedModule,
        FormsModule,
        RouterModule.forRoot([]),
        RestbaseStoreModule,
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditorComponent);
    component = fixture.componentInstance;
    component.element$ = of(Document.root());
    fixture.detectChanges();
    const injector = getTestBed();
    router = injector.get(Router);
    route = injector.get(ActivatedRoute);
  });


  it('/editor should redirect to /edit', () => {
    return fixture.ngZone.run(() => router.navigate(['/editor'])
      .then(() => {
        expect(router.navigated).toBeTruthy();
        expect(router.url).toEqual('/edit');
      }));
  });

  it('/editor/abc should redirect to /edit/abc', () => {
    return fixture.ngZone.run(() => router.navigate(['/editor/abc'])
      .then(() => {
        expect(router.navigated).toBeTruthy();
        expect(router.url).toEqual('/edit/abc');
      }));
  });

  it('/editor/123 should redirect to /edit/123', () => {
    return fixture.ngZone.run(() => router.navigate(['/editor/123'])
      .then(() => {
        expect(router.navigated).toBeTruthy();
        expect(router.url).toEqual('/edit/123');
      }));
  });

  it('/edit/abc/def should redirect to /edit/abc/def', () => {
    return fixture.ngZone.run(() => router.navigate(['/edit/abc/def'])
      .then(() => {
        expect(router.navigated).toBeTruthy();
        expect(router.url).toEqual('/edit/abc/def');
      }));
  });

  it('/edit/abc/123 should redirect to /edit/abc/123', () => {
    return fixture.ngZone.run(() => router.navigate(['/edit/abc/123'])
      .then(() => {
        expect(router.navigated).toBeTruthy();
        expect(router.url).toEqual('/edit/abc/123');
      }));
  });
});
