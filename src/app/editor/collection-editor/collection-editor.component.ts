import { Component, OnInit, Input } from '@angular/core';
import { Collection } from 'src/app/model/collection';

@Component({
  selector: 'app-collection-editor',
  templateUrl: './collection-editor.component.html',
  styleUrls: ['./collection-editor.component.sass']
})
export class CollectionEditorComponent implements OnInit {

  @Input() collection: Collection;

  constructor() { }

  ngOnInit() {
  }

}
