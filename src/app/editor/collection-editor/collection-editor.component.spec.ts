import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { SharedModule } from 'src/app/shared/shared.module';
import { DocumentListComponent } from '../document-list/document-list.component';
import { CollectionEditorComponent } from './collection-editor.component';
import { Collection } from 'src/app/model/collection';
import { ROOT } from 'src/app/model/path';
import { RestbaseStoreModule } from 'src/app/store/restbase-store.module';

describe('CollectionEditorComponent', () => {
  let component: CollectionEditorComponent;
  let fixture: ComponentFixture<CollectionEditorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        CollectionEditorComponent,
        DocumentListComponent,
      ],
      imports: [
        SharedModule,
        FormsModule,
        RouterTestingModule,
        RestbaseStoreModule,
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CollectionEditorComponent);
    component = fixture.componentInstance;
    component.collection = new Collection().deserialize({ name: 'test', parent: ROOT, id: 'testid' });
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
