import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { of } from 'rxjs';
import { Document } from 'src/app/model/document';
import { SharedModule } from '../shared/shared.module';
import { CollectionEditorComponent } from './collection-editor/collection-editor.component';
import { CollectionEditDialogComponent } from './collection-list/collection-edit-dialog.component';
import { CollectionListComponent } from './collection-list/collection-list.component';
import { DocumentEditorComponent } from './document-editor/document-editor.component';
import { EditorComponent } from './editor.component';
import { FieldListComponent } from './field-list/field-list.component';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { DocumentListComponent } from './document-list/document-list.component';
import { RestbaseStoreModule } from '../store/restbase-store.module';


describe('EditorComponent', () => {
  let component: EditorComponent;
  let fixture: ComponentFixture<EditorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        EditorComponent,
        ToolbarComponent,
        DocumentEditorComponent,
        CollectionEditorComponent,
        FieldListComponent,
        CollectionListComponent,
        CollectionEditDialogComponent,
        DocumentListComponent,
      ],
      imports: [
        SharedModule,
        FormsModule,
        RouterModule.forRoot([]),
        RestbaseStoreModule,
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditorComponent);
    component = fixture.componentInstance;
    component.element$ = of(Document.root());
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
