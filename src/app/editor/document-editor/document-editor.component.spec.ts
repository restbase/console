import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { Document } from 'src/app/model/document';
import { SharedModule } from 'src/app/shared/shared.module';
import { CollectionEditDialogComponent } from '../collection-list/collection-edit-dialog.component';
import { CollectionListComponent } from '../collection-list/collection-list.component';
import { FieldListComponent } from '../field-list/field-list.component';
import { DocumentEditorComponent } from './document-editor.component';
import { RestbaseStoreModule } from 'src/app/store/restbase-store.module';

describe('DocumentEditorComponent', () => {
  let component: DocumentEditorComponent;
  let fixture: ComponentFixture<DocumentEditorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        DocumentEditorComponent,
        FieldListComponent,
        CollectionListComponent,
        CollectionEditDialogComponent,
      ],
      imports: [
        SharedModule,
        FormsModule,
        RouterTestingModule,
        RestbaseStoreModule,
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocumentEditorComponent);
    component = fixture.componentInstance;
    component.document = Document.root();
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
