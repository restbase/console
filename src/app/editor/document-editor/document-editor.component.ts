import { Component, OnInit, Input } from '@angular/core';
import { Document } from 'src/app/model/document';

@Component({
  selector: 'app-document-editor',
  templateUrl: './document-editor.component.html',
  styleUrls: ['./document-editor.component.sass']
})
export class DocumentEditorComponent implements OnInit {

  @Input() document: Document;

  constructor() {
  }

  ngOnInit() {
  }

}
