import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { MatDialogModule, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Document } from 'src/app/model/document';
import { SharedModule } from 'src/app/shared/shared.module';
import { DocumentDeleteDialogComponent } from './document-delete-dialog.component';

describe('DocumentDeleteDialogComponent', () => {
  let component: DocumentDeleteDialogComponent;
  let fixture: ComponentFixture<DocumentDeleteDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DocumentDeleteDialogComponent],
      imports: [
        SharedModule,
        MatDialogModule,
        FormsModule,
        BrowserAnimationsModule,
      ],
      providers: [
        { provide: MatDialogRef, useValue: {} },
        {
          provide: MAT_DIALOG_DATA, useValue: {
            document: new Document().deserialize({ collection: 'test', id: 'id' }),
          }
        },
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocumentDeleteDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have data', () => {
    expect(component.data).toBeTruthy();
    expect(component.data.document).toBeTruthy();
  });

});
