import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { Collection } from 'src/app/model/collection';
import { Document } from 'src/app/model/document';
import { Path } from 'src/app/model/path';
import { AddDocument, DeleteDocument } from 'src/app/store/restbase.actions';
import { State } from '../../store';
import { DocumentDeleteDialogComponent } from './document-delete-dialog.component';

@Component({
  selector: 'app-document-list',
  templateUrl: './document-list.component.html',
  styleUrls: ['./document-list.component.sass']
})
export class DocumentListComponent implements OnInit {

  @Input() collection: Collection;

  constructor(
    private dialog: MatDialog,
    private router: Router,
    private store: Store<State>,
  ) { }

  ngOnInit() {
  }

  onAddClick() {
    this.store.dispatch(new AddDocument(this.collection));
  }

  onDocumentClick(document: Document) {
    const path = document.path();
    this.navigateTo(path);
  }

  navigateTo(path: Path) {
    const segments = ['/edit', ...path.segments];
    this.router.navigate(segments);
  }

  onDeleteClick(event: Event, document: Document) {
    // prevent parent event happening (navigation)
    event.preventDefault();
    event.stopImmediatePropagation();

    const dialogRef = this.dialog.open(DocumentDeleteDialogComponent, {
      data: {
        document: new Document().deserialize(document),
      }
    });
    dialogRef.afterClosed().subscribe((result: Document) => {
      // delete document
      if (result) {
        this.store.dispatch(new DeleteDocument(document));
      }
    });
  }

}
