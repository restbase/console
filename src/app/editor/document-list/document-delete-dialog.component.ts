import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Document } from 'src/app/model/document';

@Component({
  selector: 'app-document-delete-dialog',
  templateUrl: './document-delete-dialog.component.html',
})
export class DocumentDeleteDialogComponent implements OnInit {

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: DialogInput,
    public dialogRef: MatDialogRef<DocumentDeleteDialogComponent>,
  ) {
  }

  ngOnInit() {
  }

}

export class DialogInput {
  document: Document;
}
