import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { SharedModule } from 'src/app/shared/shared.module';
import { DocumentListComponent } from './document-list.component';
import { Collection } from 'src/app/model/collection';
import { ROOT } from 'src/app/model/path';
import { RestbaseStoreModule } from 'src/app/store/restbase-store.module';

describe('DocumentListComponent', () => {
  let component: DocumentListComponent;
  let fixture: ComponentFixture<DocumentListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DocumentListComponent],
      imports: [
        SharedModule,
        FormsModule,
        RouterTestingModule,
        RestbaseStoreModule,
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocumentListComponent);
    component = fixture.componentInstance;
    component.collection = new Collection().deserialize({ name: 'test', parent: ROOT, id: 'testid' });
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
