import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EditorComponent } from './editor.component';

export const routes: Routes = [
  { path: 'editor/:path', redirectTo: '/edit/:path' },
  { path: 'editor', redirectTo: '/edit' },
  {
    path: 'edit',
    children: [
      { path: '**', component: EditorComponent }
    ]
  },
];


@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class EditorRoutingModule { }
