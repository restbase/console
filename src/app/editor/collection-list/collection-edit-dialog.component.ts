import { Component, Inject } from '@angular/core';
import { Collection } from 'src/app/model/collection';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-collection-edit-dialog',
  templateUrl: 'collection-edit-dialog.component.html',
  styleUrls: ['./collection-edit-dialog.component.sass']
})
export class CollectionEditDialogComponent {

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: DialogInput,
    public dialogRef: MatDialogRef<CollectionEditDialogComponent>,
  ) {
  }

  onCancelClick(): void {
    this.dialogRef.close();
  }
}

export class DialogInput {
  collection: Collection;
  isNew: boolean;
}
