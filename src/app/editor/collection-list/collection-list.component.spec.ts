import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { Document } from 'src/app/model/document';
import { SharedModule } from 'src/app/shared/shared.module';
import { CollectionEditDialogComponent } from './collection-edit-dialog.component';
import { CollectionListComponent } from './collection-list.component';
import { RestbaseStoreModule } from 'src/app/store/restbase-store.module';

describe('CollectionListComponent', () => {
  let component: CollectionListComponent;
  let fixture: ComponentFixture<CollectionListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CollectionListComponent, CollectionEditDialogComponent ],
      imports: [
        SharedModule,
        FormsModule,
        RouterTestingModule,
        RestbaseStoreModule,
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CollectionListComponent);
    component = fixture.componentInstance;
    component.document = Document.root();
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
