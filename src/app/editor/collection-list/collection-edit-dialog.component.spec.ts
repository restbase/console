import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CollectionEditDialogComponent } from './collection-edit-dialog.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { MatDialogModule, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Collection } from 'src/app/model/collection';

describe('CollectionEditDialogComponent', () => {
  let component: CollectionEditDialogComponent;
  let fixture: ComponentFixture<CollectionEditDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CollectionEditDialogComponent],
      imports: [
        SharedModule,
        MatDialogModule,
        FormsModule,
        BrowserAnimationsModule,
      ],
      providers: [
        { provide: MatDialogRef, useValue: {} },
        {
          provide: MAT_DIALOG_DATA, useValue: {
            collection: new Collection().deserialize({ name: 'test' }),
            isNew: true,
          }
        },
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CollectionEditDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have data', () => {
    expect(component.data).toBeTruthy();
    expect(component.data.isNew).toBeTruthy();
    expect(component.data.collection).toBeTruthy();
    expect(component.data.collection.name).toEqual('test');
  });
});
