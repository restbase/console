import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { MatDialogModule, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Collection } from 'src/app/model/collection';
import { SharedModule } from 'src/app/shared/shared.module';
import { CollectionDeleteDialogComponent } from './collection-delete-dialog.component';

describe('CollectionDeleteDialogComponent', () => {
  let component: CollectionDeleteDialogComponent;
  let fixture: ComponentFixture<CollectionDeleteDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CollectionDeleteDialogComponent ],
      imports: [
        SharedModule,
        MatDialogModule,
        FormsModule,
        BrowserAnimationsModule,
      ],
      providers: [
        { provide: MatDialogRef, useValue: {} },
        {
          provide: MAT_DIALOG_DATA, useValue: {
            collection: new Collection().deserialize({ name: 'test' }),
          }
        },
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CollectionDeleteDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have data', () => {
    expect(component.data).toBeTruthy();
    expect(component.data.collection).toBeTruthy();
    expect(component.data.collection.name).toEqual('test');
  });

});
