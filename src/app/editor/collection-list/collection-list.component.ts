import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { Collection } from 'src/app/model/collection';
import { Document } from 'src/app/model/document';
import { Path } from 'src/app/model/path';
import { AddCollection, DeleteCollection, UpdateCollection } from 'src/app/store/restbase.actions';
import { State } from '../../store';
import { CollectionDeleteDialogComponent } from './collection-delete-dialog.component';
import { CollectionEditDialogComponent } from './collection-edit-dialog.component';

@Component({
  selector: 'app-collection-list',
  templateUrl: './collection-list.component.html',
  styleUrls: ['./collection-list.component.sass']
})
export class CollectionListComponent implements OnInit {

  @Input() document: Document;

  constructor(
    private dialog: MatDialog,
    private router: Router,
    private store: Store<State>
  ) { }

  ngOnInit() {
  }

  onAddClick() {
    const dialogRef = this.dialog.open(CollectionEditDialogComponent, {
      data: {
        collection: new Collection().deserialize({ name: '', parent: this.document.path() }),
        isNew: true,
      }
    });
    dialogRef.afterClosed().subscribe((result: Collection) => {
      // add collection
      if (result && result.name) {
        this.store.dispatch(new AddCollection(this.document, result.name));
      }
    });
  }

  onEditClick(event: Event, collection: Collection) {
    // prevent parent event happening (navigation)
    event.preventDefault();
    event.stopImmediatePropagation();

    const dialogRef = this.dialog.open(CollectionEditDialogComponent, {
      data: {
        collection: { ...collection },
        isNew: false,
      }
    });
    dialogRef.afterClosed().subscribe((result: Collection) => {
      // update collection
      if (result && result.name) {
        this.store.dispatch(new UpdateCollection(this.document, collection, result.name));
      }
    });
  }

  onDeleteClick(event: Event, collection: Collection) {
    // prevent parent event happening (navigation)
    event.preventDefault();
    event.stopImmediatePropagation();

    const dialogRef = this.dialog.open(CollectionDeleteDialogComponent, {
      data: {
        collection: new Collection().deserialize(collection),
      }
    });
    dialogRef.afterClosed().subscribe((result: Collection) => {
      // delete collection
      if (result && result.name) {
        this.store.dispatch(new DeleteCollection(collection));
      }
    });
  }

  onCollectionClick(collection: Collection) {
    const path = collection.path();
    this.navigateTo(path);
  }

  navigateTo(path: Path) {
    const segments = ['/edit', ...path.segments];
    this.router.navigate(segments);
  }

}
