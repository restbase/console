import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { map } from 'rxjs/operators';
import { path } from '../model/path';
import { State } from '../store';
import { LoadElement } from '../store/restbase.actions';
import { selectElement } from '../store/restbase.selectors';

@Component({
  selector: 'app-editor',
  templateUrl: './editor.component.html',
  styleUrls: ['./editor.component.sass']
})
export class EditorComponent implements OnInit {

  element$ = this.store.pipe(select(selectElement));

  constructor(
    private route: ActivatedRoute,
    private store: Store<State>
  ) { }

  ngOnInit() {
    this.route.url
      .pipe(
        map((segments) => segments.map(seg => seg.path).join('/')),
        map(s => path(s))
      ).subscribe(p => this.store.dispatch(new LoadElement(p)));
  }

}
