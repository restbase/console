import { Field } from './field';

describe('Field', () => {
  it('should create an instance', () => {
    expect(new Field('name')).toBeTruthy();
    expect(new Field('name', 'value')).toBeTruthy();
    expect(new Field('name', 42)).toBeTruthy();
    expect(new Field('name', true)).toBeTruthy();
  });

  it('should have a name', () => {
    expect(new Field('name').key).toEqual('name');
    expect(new Field('name', 'value').key).toEqual('name');
    expect(new Field('name', 42).key).toEqual('name');
    expect(new Field('name', true).key).toEqual('name');
  });

  it('should not have a value', () => {
    expect(new Field('name').value).toBeFalsy();
  });

  it('should have a value', () => {
    expect(new Field('name', 'value').value).toEqual('value');
    expect(new Field('name', 42).value).toEqual(42);
    expect(new Field('name', true).value).toEqual(true);
  });
});
