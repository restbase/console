import { Path, ROOT, path, isRoot } from './path';

describe('Path', () => {
  it('should create an instance', () => {
    expect(new Path([])).toBeTruthy();
    expect(new Path(['test'])).toBeTruthy();
  });

  it('should have segments', () => {
    expect(new Path(['a', 'b'])).toBeTruthy();
    expect(new Path(['a', 'b']).segments).toEqual(['a', 'b']);
  });

  it('should be joined', () => {
    expect(new Path([]).joined()).toEqual('');
    expect(new Path(['a']).joined()).toEqual('a');
    expect(new Path(['a', 'b']).joined()).toEqual('a/b');
  });

  it('should append', () => {
    expect(new Path([]).append('c')).toEqual(new Path(['c']));
    expect(new Path(['a']).append('c')).toEqual(new Path(['a', 'c']));
    expect(new Path(['a', 'b']).append('c')).toEqual(new Path(['a', 'b', 'c']));
  });

  it('should have path segments', () => {
    expect(new Path([]).pathSegments()).toEqual([]);
    expect(new Path(['a']).pathSegments()).toEqual([path('a')]);
    expect(new Path(['a', 'b']).pathSegments()).toEqual([path('a'), path('a/b')]);
  });

  it('should have last segment', () => {
    expect(new Path(['a']).lastSegment()).toEqual('a');
    expect(new Path(['a', 'b']).lastSegment()).toEqual('b');
  });

  it('should be document', () => {
    expect(path('a/b').isDocument()).toBeTruthy();
    expect(path('a/b').isCollection()).toBeFalsy();
    expect(path('a/b/c/d').isDocument()).toBeTruthy();
    expect(path('a/b/c/d').isCollection()).toBeFalsy();
    expect(path('a/b/c/d/e/f').isDocument()).toBeTruthy();
    expect(path('a/b/c/d/e/f').isCollection()).toBeFalsy();
  });

  it('should be collection', () => {
    expect(path('a').isDocument()).toBeFalsy();
    expect(path('a').isCollection()).toBeTruthy();
    expect(path('a/b/c').isDocument()).toBeFalsy();
    expect(path('a/b/c').isCollection()).toBeTruthy();
    expect(path('a/b/c/d/e').isDocument()).toBeFalsy();
    expect(path('a/b/c/d/e').isCollection()).toBeTruthy();
  });

  it('should have a parent', () => {
    expect(path('a').parent()).toEqual(ROOT);
    expect(path('a/b').parent()).toEqual(path('a'));
    expect(path('a/b/c').parent()).toEqual(path('a/b'));
    expect(path('a/b/c/d').parent()).toEqual(path('a/b/c'));
    expect(path('a/b/c/d/e').parent()).toEqual(path('a/b/c/d'));
  });

});

describe('ROOT', () => {
  it('should be path without segments', () => {
    expect(ROOT).toEqual(new Path([]));
    expect(ROOT.segments).toEqual([]);
  });

  it('should be joined as empty string', () => {
    expect(ROOT.joined()).toEqual('');
  });

  it('should append', () => {
    expect(ROOT.append('a')).toEqual(new Path(['a']));
  });

  it('should not have a lastSegment', () => {
    expect(ROOT.lastSegment()).toBeUndefined();
  });

  it('should be document', () => {
    expect(ROOT.isDocument()).toBeTruthy();
  });

  it('should NOT be collection', () => {
    expect(ROOT.isCollection()).toBeFalsy();
  });

  it('should have itself as parent', () => {
    expect(ROOT.parent()).toEqual(ROOT);
  });
});

describe('path()', () => {
  it('should return ROOT if no segments provided', () => {
    expect(path()).toEqual(ROOT);
    expect(path(undefined)).toEqual(ROOT);
    expect(path(null)).toEqual(ROOT);
    expect(path('')).toEqual(ROOT);
  });

  it('should return Path with segments', () => {
    expect(path('a')).toEqual(new Path(['a']));
    expect(path('a/b')).toEqual(new Path(['a', 'b']));
  });
});

describe('isRoot()', () => {
  it('should be true if empty/invalid path provided', () => {
    expect(isRoot()).toBeTruthy();
    expect(isRoot(undefined)).toBeTruthy();
    expect(isRoot(null)).toBeTruthy();
    expect(isRoot('')).toBeTruthy();
  });

  it('should be false if some path provided', () => {
    expect(isRoot('a')).toBeFalsy();
    expect(isRoot('a/b')).toBeFalsy();
  });

});
