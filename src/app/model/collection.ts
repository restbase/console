import { Document } from './document';
import { Path, ROOT } from './path';
import { Deserializable } from './deserializable';

export class Collection implements Deserializable<Collection> {
  public name: string;
  public parent: Path;
  public id: string;
  public documents: Document[];

  deserialize(input: Partial<Collection>): Collection {
    Object.assign(this, input);
    this.documents = input.documents ?
      input.documents.map(d => new Document().deserialize(d)) : [];
    this.parent = input.parent ? new Path(input.parent.segments) : ROOT;
    return this;
  }

  path(): Path {
    return this.parent.append(this.name);
  }

  getParent(): Path {
    return this.parent;
  }

  segments(): Path[] {
    return this.path().pathSegments();
  }

  isDocument(): boolean {
    return false;
  }

  isCollection(): boolean {
    return true;
  }

}
