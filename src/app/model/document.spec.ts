import { ROOT, path } from './path';
import { Document, } from './document';
import { Collection } from './collection';
import { FieldType } from './field';
const root = Document.root;

describe('Document', () => {
  it('should have a root', () => {
    expect(root()).toBeTruthy();
  });

  it('should create an instance', () => {
    expect(new Document()).toBeTruthy();
    expect(new Document().deserialize({ collection: 'a' })).toBeTruthy();
  });

  it('should have initial id', () => {
    expect(new Document().deserialize({ id: 'a' }).id).toEqual('a');
  });

  it('should have initial parent', () => {
    expect(new Document().deserialize({ collection: 'a' }).getParent()).toEqual(path('a'));
    expect(new Document().deserialize({ collection: 'a/b' }).getParent()).toEqual(path('a/b'));
  });

  it('should have initial fields', () => {
    expect(new Document().fields).toBeTruthy();
    expect(new Document().deserialize({}).fields).toBeTruthy();
    expect(new Document().deserialize({ fields: {} }).fields).toEqual({});
    expect(new Document().deserialize({ fields: { key: 42 } }).get('key')).toEqual(42);
  });

  it('should return existing field value', () => {
    expect(new Document().set('key', 42).get('key')).toEqual(42);
    expect(new Document().set('key', '42').get('key')).toEqual('42');
  });

  it('should miss missing field value', () => {
    expect(new Document().get('key')).toBeFalsy();
  });

  it('should set and get the same field value', () => {
    expect(new Document().set('key', 42).get('key')).toEqual(42);
    expect(new Document().set('key', '42').get('key')).toEqual('42');
  });

  it('should return the same document after chained set of a field value', () => {
    const doc = new Document();
    expect(doc.set('key', 42)).toBe(doc);
  });

  it('should have a single ROOT document', () => {
    expect(root()).toBe(root());
  });

  it('should have a ROOT path if it"s a ROOT document', () => {
    expect(root().path()).toEqual(ROOT);
  });

  it('should have a path ', () => {
    expect(new Document().path()).toEqual(ROOT);
    expect(new Document().deserialize({ id: 'id', collection: 'col' }).path()).toEqual(path('col/id'));
  });

  it('should have a parent path if not ROOT', () => {
    expect(new Document().deserialize({ id: 'id', collection: 'col' }).getParent()).toEqual(path('col'));
  });

  it('should not have a parent path if ROOT', () => {
    expect(root().getParent()).toBeUndefined();
  });

  it('.isRoot() should return true for ROOT document', () => {
    expect(root().isRoot()).toBeTruthy();
  });

  it('.isRoot() should return false for child document', () => {
    expect(new Document().deserialize({ id: 'id', collection: 'col' }).isRoot()).toBeFalsy();
  });

  it('at root should NOT have segments', () => {
    expect(root().segments()).toEqual([]);
  });

  it('should have segments', () => {
    expect(new Document().deserialize({ id: 'id', collection: 'col' }).segments())
      .toEqual([path('col'), path('col/id')]);
  });

  it('should serialize / deserialize when empty', () => {
    const doc = new Document();
    const serialized = JSON.stringify(doc);
    const parsed = new Document().deserialize(JSON.parse(serialized));
    expect(parsed).not.toBeNull();
    expect(parsed).toEqual(doc);
  });

  it('should serialize / deserialize with fields', () => {
    const collections = [new Collection().deserialize({ name: 'just-testing' })];
    const fields = { int: 42, str: 'val' };
    const doc = new Document()
      .deserialize({
        id: 'testid',
        collection: 'testcollection',
        collections,
        fields
      });
    const serialized = JSON.stringify(doc).replace('"fields":{}', `"fields":{"int": 42, "str": "val"}`);
    const parsed = JSON.parse(serialized);
    const doc2 = new Document().deserialize(parsed);
    expect(doc2).not.toBeNull();
    expect(doc2.fields).toEqual(fields);
    expect(doc2.collections).toEqual(collections);
    expect(doc2).toEqual(doc);
  });

});
