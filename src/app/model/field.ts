export type FieldType = number | string | boolean;

export class Field {
  constructor(
    public key: string,
    public value?: FieldType
  ) { }
}
