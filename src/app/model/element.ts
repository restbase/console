import { Collection } from './collection';
import { Document } from './document';

export type Element
  = Document
  | Collection
  ;
