import { Collection } from './collection';
import { Document } from './document';
import { ROOT, path } from './path';

describe('Collection', () => {
  it('should create an instance', () => {
    expect(new Collection()).toBeTruthy();
    expect(new Collection().deserialize({ name: 'test', parent: ROOT })).toBeTruthy();
  });

  it('constructor should set a name', () => {
    expect(new Collection().deserialize({ name: 'test' }).name).toEqual('test');
  });

  it('constructor should set a parent', () => {
    expect(new Collection().deserialize({ name: 'test', parent: ROOT }).parent).toEqual(ROOT);
    expect(new Collection().deserialize({ name: 'test', parent: path('a') }).parent).toEqual(path('a'));
  });

  it('constructor should set an id', () => {
    expect(new Collection().deserialize({ name: 'test', parent: ROOT, id: '42' }).id).toEqual('42');
  });

  it('should have a parent', () => {
    expect(new Collection().deserialize({ name: 'test', parent: ROOT }).getParent()).toEqual(ROOT);
    expect(new Collection().deserialize({ name: 'test', parent: path('a') }).getParent()).toEqual(path('a'));
  });

  it('should have a path', () => {
    expect(new Collection().deserialize({ name: 'test', parent: ROOT }).path()).toEqual(path('test'));
    expect(new Collection().deserialize({ name: 'test', parent: path('a') }).path()).toEqual(path('a/test'));
  });

  it('should have segments', () => {
    expect(new Collection().deserialize({ name: 'test', parent: ROOT }).segments())
      .toEqual([path('test')]);
    expect(new Collection().deserialize({ name: 'test', parent: path('a') }).segments())
      .toEqual([path('a'), path('a/test')]);
  });

  it('should serialize / deserialize when empty', () => {
    const col = new Collection().deserialize({ parent: ROOT });
    const serialized = JSON.stringify(col);
    const parsed = new Collection().deserialize(JSON.parse(serialized));
    expect(parsed).not.toBeNull();
    expect(parsed).toEqual(col);
  });

  it('should serialize / deserialize with fields', () => {
    const documents = [
      new Document()
        .deserialize({
          id: 'test-doc'
        })
    ];
    const col = new Collection()
      .deserialize({
        parent: ROOT,
        id: 'test-id',
        name: 'test-name',
        documents
      });

    const serialized = JSON.stringify(col);
    const parsed = new Collection()
      .deserialize(JSON.parse(serialized));
    expect(parsed).not.toBeNull();
    expect(parsed).toEqual(col);
    expect(parsed.documents).toEqual(documents);
  });

});
