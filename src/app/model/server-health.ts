export enum Status {
  UP = 'UP',
  DOWN = 'DOWN',
}

export interface ServerHealth {
  status: Status;
}
