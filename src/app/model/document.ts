import { Collection } from './collection';
import { FieldType } from './field';
import { Path, path, ROOT as R } from './path';
import { Deserializable, map } from './deserializable';

class FieldMap {
  [prop: string]: FieldType;
}
export class Document implements Deserializable<Document> {
  static ROOT = new Document();

  public collection: string;
  public id: string;
  public collections: Collection[];
  public fields: FieldMap;

  constructor() {
    this.collections = [];
    this.fields = {};
  }

  public static root(): Document {
    return Document.ROOT;
  }

  deserialize(input: Partial<Document>): Document {
    Object.assign(this, input);
    this.collections = input.collections ? input.collections.map((c) => new Collection().deserialize(c)) : [];
    this.fields = input.fields ? input.fields : {};
    return this;
  }

  public get(field: string): FieldType | undefined {
    return this.fields[field];
  }

  public set(field: string, value: FieldType): Document {
    this.fields = {
      ...this.fields,
      [field]: value,
    };
    return this;
  }

  public delete(field: string): Document {
    delete this.fields[field];
    return this;
  }

  public path(): Path {
    if (this.isRoot()) {
      return R;
    }
    return this.getParent().append(this.id);
  }

  public getParent(): Path {
    if (this.isRoot()) {
      return undefined;
    }
    return path(this.collection);
  }

  public isRoot(): boolean {
    return this.collection === undefined;
  }

  segments(): Path[] {
    return this.path().pathSegments();
  }

  isDocument(): boolean {
    return true;
  }

  isCollection(): boolean {
    return false;
  }

}

