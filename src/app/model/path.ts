const SEPARATOR = '/';

export class Path {

  constructor(public segments: string[]) {
  }

  public isRoot(): boolean {
    return !this.segments || this.segments.length === 0;
  }

  public append(segment: string): Path {
    return new Path([...this.segments, segment]);
  }

  joined(): string {
    return this.segments.join(SEPARATOR);
  }

  pathSegments(): Path[] {
    const result = [];
    let parent = ROOT;
    this.segments.forEach(segment => {
      parent = parent.append(segment);
      result.push(parent);
    });
    return result;
  }

  lastSegment(): string {
    return (this.isRoot())
      ? undefined
      : this.segments[this.segments.length - 1];
  }

  parent(): Path {
    if (this.segments.length < 2) {
      return ROOT;
    }
    const lastIndex = this.segments.length - 1;
    const segments = this.segments.slice(0, lastIndex);
    return new Path(segments);
  }

  isDocument(): boolean {
    return this.segments.length % 2 === 0;
  }

  isCollection(): boolean {
    return this.segments.length % 2 !== 0;
  }

}

export const ROOT = new Path([]);

export function path(p?: string): Path {
  if (isRoot(p)) {
    return ROOT;
  }
  const segments = p.split(SEPARATOR);
  return new Path(segments);
}

export function isRoot(p?: string): boolean {
  return !p || p === '';
}
