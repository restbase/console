export interface Deserializable<T> {
  deserialize(input: Partial<T>): T;
}

export function map<V>(input: any): Map<string, V> {
  const m = new Map();
  if (input) {
    if (input.forEach) {
      // input is a map
      input.forEach((v: V, k: string) => {
        m.set(k, v);
      });
    } else {
      // input is an object
      for (const k of Object.keys(input)) {
        const v = input[k];
        m.set(k, v);
      }
    }
  }

  return m;
}
