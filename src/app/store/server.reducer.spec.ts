import { reducer, initialState, ServerStatus } from './server.reducer';
import { CheckStatusUp, CheckStatusDown, CheckStatus } from './server.actions';

describe('Server Reducer', () => {
  describe('an unknown action', () => {
    it('should return the previous state', () => {
      const action = {} as any;
      const result = reducer(initialState, action);
      expect(result).toBe(initialState);
    });
  });

  describe('check status', () => {
    it('should return server check status', () => {
      const action = new CheckStatus();
      const result = reducer(initialState, action);
      expect(result).toBe(initialState);
    });
  });

  describe('server is up', () => {
    it('should return server status up', () => {
      const action = new CheckStatusUp();
      const result = reducer(initialState, action);
      expect(result.status).toBe(ServerStatus.UP);
    });
  });

  describe('server is down', () => {
    it('should return server status down', () => {
      const action = new CheckStatusDown();
      const result = reducer(initialState, action);
      expect(result.status).toBe(ServerStatus.DOWN);
    });
  });

});
