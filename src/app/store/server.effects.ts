import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';
import { ServerHealth, Status } from '../model/server-health';
import { RestbaseService } from '../restbase/restbase.service';
import { CheckStatus, CheckStatusDown, CheckStatusUp, ServerActionTypes } from './server.actions';

@Injectable()
export class ServerEffects {

  @Effect()
  checkStatus$ = this.actions$.pipe(
    ofType<CheckStatus>(ServerActionTypes.CheckStatus),
    switchMap(() => this.restbase.getHealth()
      .pipe(
        map((health: ServerHealth) =>
          health.status === Status.UP
            ? new CheckStatusUp()
            : new CheckStatusDown()),
        catchError(() => of(new CheckStatusDown())),
      )),
  );

  constructor(
    private actions$: Actions,
    private restbase: RestbaseService,
  ) { }

}
