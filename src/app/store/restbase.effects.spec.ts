import { HttpClientModule } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { Actions } from '@ngrx/effects';
import { provideMockActions } from '@ngrx/effects/testing';
import { cold } from 'jasmine-marbles';
import { of, throwError } from 'rxjs';
import { Document } from '../model/document';
import { path, ROOT } from '../model/path';
import {
  LoadElement, LoadElementSuccess, LoadElementFail,
  AddDocument, AddDocumentSuccess, AddDocumentFail,
  UpdateDocument, UpdateDocumentSuccess, UpdateDocumentFail,
  AddCollection, AddCollectionSuccess, AddCollectionFail,
  UpdateCollection, UpdateCollectionSuccess, UpdateCollectionFail,
  DeleteCollection, DeleteCollectionSuccess, DeleteCollectionFail, DeleteDocument, DeleteDocumentSuccess, DeleteDocumentFail,
} from './restbase.actions';
import { RestbaseEffects } from './restbase.effects';
import { Collection } from '../model/collection';

const getElementStub = (response: any) => {
  const service = jasmine.createSpyObj('restbase', ['getElement']);
  const isError = response instanceof Error;
  const serviceResponse = isError ? throwError(response) : of(response);
  service.getElement.and.returnValue(serviceResponse);
  return service;
};

const addCollectionStub = (response: any) => {
  const service = jasmine.createSpyObj('restbase', ['addCollection']);
  const isError = response instanceof Error;
  const serviceResponse = isError ? throwError(response) : of(response);
  service.addCollection.and.returnValue(serviceResponse);
  return service;
};

const updateCollectionStub = (response: any) => {
  const service = jasmine.createSpyObj('restbase', ['updateCollection']);
  const isError = response instanceof Error;
  const serviceResponse = isError ? throwError(response) : of(response);
  service.updateCollection.and.returnValue(serviceResponse);
  return service;
};

const deleteCollectionStub = (response: any) => {
  const service = jasmine.createSpyObj('restbase', ['deleteCollection']);
  const isError = response instanceof Error;
  const serviceResponse = isError ? throwError(response) : of(response);
  service.deleteCollection.and.returnValue(serviceResponse);
  return service;
};

const addDocumentStub = (response: any) => {
  const service = jasmine.createSpyObj('restbase', ['addDocument']);
  const isError = response instanceof Error;
  const serviceResponse = isError ? throwError(response) : of(response);
  service.addDocument.and.returnValue(serviceResponse);
  return service;
};

const updateDocumentStub = (response: any) => {
  const service = jasmine.createSpyObj('restbase', ['updateDocument']);
  const isError = response instanceof Error;
  const serviceResponse = isError ? throwError(response) : of(response);
  service.updateDocument.and.returnValue(serviceResponse);
  return service;
};

const deleteDocumentStub = (response: any) => {
  const service = jasmine.createSpyObj('restbase', ['deleteDocument']);
  const isError = response instanceof Error;
  const serviceResponse = isError ? throwError(response) : of(response);
  service.deleteDocument.and.returnValue(serviceResponse);
  return service;
};

describe('RestbaseEffects', () => {

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientModule,
      ],
      providers: [
        RestbaseEffects,
        provideMockActions(() => of({}))
      ]
    });

  });

  it('should be created', () => {
    const effects = TestBed.get<RestbaseEffects>(RestbaseEffects);
    expect(effects).toBeTruthy();
  });

  it('loadElement should return LoadElementSuccess', () => {
    const doc = new Document().deserialize({ collection: 'a' });
    const p = path('a/b');
    const source = cold('a', { a: new LoadElement(p) });
    const service = getElementStub(doc);
    const effects = new RestbaseEffects(new Actions(source), service);

    const expected = cold('a', { a: new LoadElementSuccess(p, doc) });
    expect(effects.loadElement$).toBeObservable(expected);
  });

  it('loadElement should return LoadElementFail for error', () => {
    const p = path('a/b');
    const source = cold('a', { a: new LoadElement(p) });
    const service = getElementStub(new Error('unknown error'));
    const effects = new RestbaseEffects(new Actions(source), service);

    const expected = cold('a', { a: new LoadElementFail(p) });
    expect(effects.loadElement$).toBeObservable(expected);
  });

  it('addDocument should return AddDocumentSuccess', () => {
    const col = new Collection().deserialize({ name: 'test' });
    const doc = new Document().deserialize({ collection: col.path().joined() });

    const source = cold('a', { a: new AddDocument(col) });
    const service = addDocumentStub(doc);
    const effects = new RestbaseEffects(new Actions(source), service);

    const expected = cold('a', { a: new AddDocumentSuccess(col, doc) });
    expect(effects.addDocument$).toBeObservable(expected);
  });

  it('addDocument should return AddDocumentFail for error', () => {
    const col = new Collection().deserialize({ name: 'test' });
    const source = cold('a', { a: new AddDocument(col) });

    const service = addDocumentStub(new Error('unknown error'));
    const effects = new RestbaseEffects(new Actions(source), service);

    const expected = cold('a', { a: new AddDocumentFail(col) });
    expect(effects.addDocument$).toBeObservable(expected);
  });

  it('updateDocument should return UpdateDocumentSuccess', () => {
    const col = new Collection().deserialize({ parent: ROOT, name: 'test' });
    const doc = new Document().deserialize({ collection: col.path().joined() });

    const source = cold('a', { a: new UpdateDocument(doc.path(), doc) });
    const service = updateDocumentStub(doc);
    const effects = new RestbaseEffects(new Actions(source), service);

    const expected = cold('a', { a: new UpdateDocumentSuccess(doc) });
    expect(effects.updateDocument$).toBeObservable(expected);
  });

  it('updateDocument should return UpdateDocumentFail for error', () => {
    const col = new Collection().deserialize({ parent: ROOT, name: 'test' });
    const doc = new Document().deserialize({ collection: col.path().joined() });
    const source = cold('a', { a: new UpdateDocument(doc.path(), doc) });

    const service = updateDocumentStub(new Error('unknown error'));
    const effects = new RestbaseEffects(new Actions(source), service);

    const expected = cold('a', { a: new UpdateDocumentFail(doc.path(), doc) });
    expect(effects.updateDocument$).toBeObservable(expected);
  });

  it('deleteDocument should return DeleteDocumentSuccess', () => {
    const doc = new Document().deserialize({ id: 'test' });

    const source = cold('a', { a: new DeleteDocument(doc) });
    const service = deleteDocumentStub(doc);
    const effects = new RestbaseEffects(new Actions(source), service);

    const expected = cold('a', { a: new DeleteDocumentSuccess(doc) });
    expect(effects.deleteDocument$).toBeObservable(expected);
  });

  it('deleteDocument should return DeleteDocumentFail for error', () => {
    const doc = new Document().deserialize({ id: 'test' });
    const source = cold('a', { a: new DeleteDocument(doc) });

    const service = deleteDocumentStub(new Error('unknown error'));
    const effects = new RestbaseEffects(new Actions(source), service);

    const expected = cold('a', { a: new DeleteDocumentFail(doc) });
    expect(effects.deleteDocument$).toBeObservable(expected);
  });

  it('addCollection should return AddCollectionSuccess', () => {
    const doc = new Document().deserialize({ collection: 'a' });
    const source = cold('a', { a: new AddCollection(doc, 'test') });
    const collection = new Collection().deserialize({ parent: doc.path(), name: 'test' });
    const service = addCollectionStub(collection);
    const effects = new RestbaseEffects(new Actions(source), service);

    const expected = cold('a', { a: new AddCollectionSuccess(doc, collection) });
    expect(effects.addCollection$).toBeObservable(expected);
  });

  it('addCollection should return AddCollectionFail for error', () => {
    const doc = new Document().deserialize({ collection: 'a', id: 'testId' });
    const source = cold('a', { a: new AddCollection(doc, 'error') });
    const service = addCollectionStub(new Error('unknown error'));
    const effects = new RestbaseEffects(new Actions(source), service);

    const expected = cold('a', { a: new AddCollectionFail(doc, 'error') });
    expect(effects.addCollection$).toBeObservable(expected);
  });

  it('updateCollection should return UpdateCollectionSuccess', () => {
    const col1 = new Collection().deserialize({ parent: path('a/test'), name: 'col1' });
    const doc = new Document().deserialize({ collection: 'a', id: 'test', collections: [col1] });
    const source = cold('a', { a: new UpdateCollection(doc, col1, 'col2') });
    const col2 = new Collection().deserialize({ parent: path('a/test'), name: 'col2' });
    const service = updateCollectionStub(col2);
    const effects = new RestbaseEffects(new Actions(source), service);

    const expected = cold('a', { a: new UpdateCollectionSuccess(doc, col1, col2) });
    expect(effects.updateCollection$).toBeObservable(expected);
  });

  it('updateCollection should return UpdateCollectionFail for error', () => {
    const col1 = new Collection().deserialize({ parent: path('a/test'), name: 'col1' });
    const doc = new Document().deserialize({ collection: 'a', id: 'test', collections: [col1] });
    const source = cold('a', { a: new UpdateCollection(doc, col1, 'col2') });
    const service = updateCollectionStub(new Error('unknown error'));
    const effects = new RestbaseEffects(new Actions(source), service);

    const expected = cold('a', { a: new UpdateCollectionFail(doc, col1, 'col2') });
    expect(effects.updateCollection$).toBeObservable(expected);
  });

  it('deleteCollection should return DeleteCollectionSuccess', () => {
    const col = new Collection().deserialize({ parent: ROOT, name: 'col1' });
    const source = cold('a', { a: new DeleteCollection(col) });
    const service = deleteCollectionStub([]);
    const effects = new RestbaseEffects(new Actions(source), service);

    const expected = cold('a', { a: new DeleteCollectionSuccess([]) });
    expect(effects.deleteCollection$).toBeObservable(expected);
  });

  it('deleteCollection should return DeleteCollectionFail for error', () => {
    const col = new Collection().deserialize({ parent: ROOT, name: 'col1' });
    const source = cold('a', { a: new DeleteCollection(col) });
    const service = deleteCollectionStub(new Error('unknown error'));
    const effects = new RestbaseEffects(new Actions(source), service);

    const expected = cold('a', { a: new DeleteCollectionFail(col) });
    expect(effects.deleteCollection$).toBeObservable(expected);
  });

});
