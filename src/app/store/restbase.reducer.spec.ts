import { Collection } from '../model/collection';
import { Document } from '../model/document';
import { path } from '../model/path';
import {
  LoadElement, LoadElementFail, LoadElementSuccess,
  AddDocument, AddDocumentFail, AddDocumentSuccess,
  UpdateDocument, UpdateDocumentFail, UpdateDocumentSuccess,
  DeleteDocument, DeleteDocumentSuccess, DeleteDocumentFail,
  AddCollection, AddCollectionSuccess, AddCollectionFail,
  UpdateCollection, UpdateCollectionSuccess, UpdateCollectionFail,
  DeleteCollection, DeleteCollectionSuccess, DeleteCollectionFail,
} from './restbase.actions';
import { initialState, reducer } from './restbase.reducer';

describe('Restbase Reducer', () => {
  describe('an unknown action', () => {
    it('should return the previous state', () => {
      const action = {} as any;
      const result = reducer(initialState, action);
      expect(result).toBe(initialState);
    });
  });

  describe('LoadElement action', () => {
    it('should return the previous state', () => {
      const action = new LoadElement(path('a/b'));
      const result = reducer(initialState, action);
      expect(result).toEqual(initialState);
    });
  });

  describe('LoadElementSuccess action', () => {
    it('should return the state with a new document', () => {
      const doc = new Document().deserialize({ collection: 'a' });
      const action = new LoadElementSuccess(path('a/b'), doc);
      const result = reducer(initialState, action);
      expect(result.element).toEqual(doc);
    });
  });

  describe('LoadElementFail action', () => {
    it('should return the initial state', () => {
      const action = new LoadElementFail(path('a/b'));
      const result = reducer(initialState, action);
      expect(result).toEqual(initialState);
    });
  });

  describe('LoadElementFail action', () => {
    it('should return the previous state', () => {
      const doc = new Document().deserialize({ collection: 'a' });
      const state = { element: doc };
      const action = new LoadElementFail(path('a/b'));
      const result = reducer(state, action);
      expect(result).toEqual(state);
      expect(result.element).toEqual(doc);
    });
  });

  describe('AddDocument action', () => {
    it('should return the previous state', () => {
      const col = new Collection().deserialize({ name: 'test' });
      const action = new AddDocument(col);
      const result = reducer(initialState, action);
      expect(result).toEqual(initialState);
    });
  });

  describe('AddDocumentSuccess action', () => {
    it('should return the state with collection (even if initial element was undefined)', () => {
      const doc = new Document().deserialize({ collection: 'a' });
      const col = new Collection().deserialize({ parent: doc.path(), name: 'test' });
      const action = new AddDocumentSuccess(col, doc);
      const result = reducer(initialState, action);
      expect(result.element).toEqual(col);
      expect((result.element as Collection).documents).toContain(doc);
    });

    it('should return the state with collection (initial element is a collection)', () => {
      const doc = new Document().deserialize({ collection: 'a' });
      const col = new Collection().deserialize({ parent: doc.path(), name: 'test' });
      const initialStateWithCollection = { element: col };
      const action = new AddDocumentSuccess(col, doc);
      const result = reducer(initialStateWithCollection, action);
      expect(result.element).toEqual(col);
      expect((result.element as Collection).documents).toContain(doc);
    });

    it('should return initial state if initial element is a document', () => {
      const col = new Collection().deserialize({ name: 'test' });
      const doc = new Document().deserialize({ collection: 'a' });
      const initialStateWithDocument = { element: doc };
      const action = new AddDocumentSuccess(col, doc);
      const result = reducer(initialStateWithDocument, action);
      expect(result).toEqual(initialStateWithDocument);
    });
  });

  describe('AddDocumentFail action', () => {
    it('should return the previous state', () => {
      const col = new Collection().deserialize({ name: 'test' });
      const state = { element: col };
      const action = new AddDocumentFail(col);
      const result = reducer(state, action);
      expect(result).toEqual(state);
      expect(result.element).toEqual(col);
    });
  });

  describe('UpdateDocument action', () => {
    it('should return the previous state', () => {
      const col = new Collection().deserialize({ name: 'test' });
      const doc = new Document().deserialize({ collection: col.path().joined() });
      const action = new UpdateDocument(doc.path(), doc);
      const result = reducer(initialState, action);
      expect(result).toEqual(initialState);
    });
  });

  describe('UpdateDocumentSuccess action', () => {
    it('should return the state with document (even if initial element was undefined)', () => {
      const col = new Collection().deserialize({ name: 'test' });
      const doc = new Document().deserialize({ collection: col.path().joined() });
      const action = new UpdateDocumentSuccess(doc);
      const result = reducer(initialState, action);
      expect(result.element).toEqual(doc);
    });

    it('should return the initial state with collection (initial element is a collection)', () => {
      const col = new Collection().deserialize({ name: 'test' });
      const doc = new Document().deserialize({ collection: col.path().joined() });
      const initialStateWithCollection = { element: col };
      const action = new UpdateDocumentSuccess(doc);
      const result = reducer(initialStateWithCollection, action);
      expect(result.element).toEqual(col);
    });

    it('should return initial state if initial element is a document', () => {
      const col = new Collection().deserialize({ name: 'test' });
      const doc = new Document().deserialize({ collection: col.path().joined() });
      const doc2 = new Document().deserialize({ collection: col.path().joined(), fields: {} });
      const initialStateWithDocument = { element: doc };
      const action = new UpdateDocumentSuccess(doc2);
      const result = reducer(initialStateWithDocument, action);
      expect(result).toEqual(initialStateWithDocument);
    });
  });

  describe('UpdateDocumentFail action', () => {
    it('should return the previous state', () => {
      const col = new Collection().deserialize({ name: 'test' });
      const doc = new Document().deserialize({ collection: col.path().joined() });
      const state = { element: col };
      const action = new UpdateDocumentFail(doc.path(), doc);
      const result = reducer(state, action);
      expect(result).toEqual(state);
      expect(result.element).toEqual(col);
    });
  });

  describe('DeleteDocument action', () => {
    it('should return the previous state', () => {
      const col = new Collection().deserialize({ name: 'test' });
      const doc = new Document().deserialize({ collection: col.path().joined() });
      const action = new DeleteDocument(doc);
      const result = reducer(initialState, action);
      expect(result).toEqual(initialState);
    });
  });

  describe('DeleteDocumentSuccess action', () => {
    it('should return the initial state with initial state', () => {
      const col = new Collection().deserialize({ name: 'test' });
      const doc = new Document().deserialize({ collection: col.path().joined() });
      const action = new DeleteDocumentSuccess(doc);
      const result = reducer(initialState, action);
      expect(result).toEqual(initialState);
    });

    it('should return the state without the deleted document if initial element is a collection', () => {
      const doc = new Document().deserialize({ id: 'testdoc', collection: 'test' });
      const col = new Collection().deserialize({ name: 'test' });
      const initialStateWithCollection = { element: col };
      const action = new DeleteDocumentSuccess(doc);
      const result = reducer(initialStateWithCollection, action);
      expect(result.element).toEqual(col);
    });

    it('should return the initial state if initial element is a document', () => {
      const col = new Collection().deserialize({ name: 'test' });
      const doc = new Document().deserialize({ collection: col.path().joined() });
      const doc2 = new Document().deserialize({ collection: col.path().joined(), fields: {} });
      const initialStateWithDocument = { element: doc };
      const action = new DeleteDocumentSuccess(doc2);
      const result = reducer(initialStateWithDocument, action);
      expect(result).toEqual(initialStateWithDocument);
    });
  });

  describe('DeleteDocumentFail action', () => {
    it('should return the previous state', () => {
      const col = new Collection().deserialize({ name: 'test' });
      const doc = new Document().deserialize({ collection: col.path().joined() });
      const state = { element: col };
      const action = new DeleteDocumentFail(doc);
      const result = reducer(state, action);
      expect(result).toEqual(state);
      expect(result.element).toEqual(col);
    });
  });

  describe('AddCollection action', () => {
    it('should return the previous state', () => {
      const doc = new Document().deserialize({ collection: 'a' });
      const action = new AddCollection(doc, 'test');
      const result = reducer(initialState, action);
      expect(result).toEqual(initialState);
    });
  });

  describe('AddCollectionSuccess action', () => {
    it('should return the the state with a new document containing the collection with initial state', () => {
      const doc = new Document().deserialize({ collection: 'a' });
      const col = new Collection().deserialize({ parent: doc.path(), name: 'test' });
      const action = new AddCollectionSuccess(doc, col);
      const result = reducer(initialState, action);
      expect((result.element as Document).collections).toContain(col);
    });

    it('should return the the state with a new document containing the collection with initial state with a document', () => {
      const doc = new Document().deserialize({ collection: 'a' });
      const initialStateWithDocument = { element: doc };
      const col = new Collection().deserialize({ parent: doc.path(), name: 'test' });
      const action = new AddCollectionSuccess(doc, col);
      const result = reducer(initialStateWithDocument, action);
      expect((result.element as Document).collections).toContain(col);
    });

    it('should return the the state with a new document containing the collection with initial state with a collection', () => {
      const initialStateWithCollection = { element: new Collection() };
      const doc = new Document().deserialize({ collection: 'a' });
      const col = new Collection().deserialize({ parent: doc.path(), name: 'test' });
      const action = new AddCollectionSuccess(doc, col);
      const result = reducer(initialStateWithCollection, action);
      expect(result).toEqual(initialStateWithCollection);
    });
  });

  describe('AddCollectionFail action', () => {
    it('should return the previous state', () => {
      const doc = new Document().deserialize({ collection: 'a' });
      const state = { element: doc };
      const action = new AddCollectionFail(doc, 'fail');
      const result = reducer(state, action);
      expect(result).toEqual(state);
      expect(result.element).toEqual(doc);
    });
  });

  describe('UpdateCollection action', () => {
    it('should return the previous state', () => {
      const col = new Collection();
      const doc = new Document().deserialize({ collection: 'a' });
      const action = new UpdateCollection(doc, col, 'test');
      const result = reducer(initialState, action);
      expect(result).toEqual(initialState);
    });
  });

  describe('UpdateCollectionSuccess action', () => {

    it('should return the state with document containing the updated collection', () => {
      const col1 = new Collection().deserialize({ parent: path('a/test'), name: 'col1', id: 'col1' });
      const col2 = new Collection().deserialize({ parent: path('a/test'), name: 'col2', id: 'col2' });
      const doc = new Document().deserialize({ collection: 'a', id: 'test', collections: [col1] });
      const action = new UpdateCollectionSuccess(doc, col1, col2);
      const result = reducer(initialState, action);
      expect((result.element as Document).collections).toContain(col2);
      expect((result.element as Document).collections).not.toContain(col1);
    });


    it('should return initial state if the old collection was not in document', () => {
      const col1 = new Collection().deserialize({ parent: path('a/test'), name: 'col1', id: 'col1' });
      const col2 = new Collection().deserialize({ parent: path('a/test'), name: 'col2', id: 'col2' });
      const doc = new Document().deserialize({ collection: 'a', id: 'test', collections: [new Collection()] });
      const action = new UpdateCollectionSuccess(doc, col1, col2);
      const result = reducer(initialState, action);
      expect(result).toEqual(initialState);
    });

    it('should return the state with document without the new collection if the old collection was not in document', () => {
      const col1 = new Collection().deserialize({ parent: path('a/test'), name: 'col1', id: 'col1' });
      const col2 = new Collection().deserialize({ parent: path('a/test'), name: 'col2', id: 'col2' });
      const doc = new Document().deserialize({ collection: 'a', id: 'test', collections: [new Collection()] });
      const initialStateWithDocument = { element: doc };
      const action = new UpdateCollectionSuccess(doc, col1, col2);
      const result = reducer(initialStateWithDocument, action);
      expect((result.element as Document).collections).not.toContain(col1);
      expect((result.element as Document).collections).not.toContain(col2);
    });
  });

  describe('UpdateCollectionFail action', () => {
    it('should return the previous state', () => {
      const col1 = new Collection().deserialize({ parent: path('a/test'), name: 'col1' });
      const doc = new Document().deserialize({ collection: 'a', id: 'test', collections: [col1] });
      const state = { element: doc };
      const action = new UpdateCollectionFail(doc, col1, 'fail');
      const result = reducer(state, action);
      expect(result).toEqual(state);
      expect(result.element).toEqual(doc);
    });
  });

  describe('DeleteCollection action', () => {
    it('should return the previous state', () => {
      const col = new Collection().deserialize({ parent: path('test'), name: 'col' });
      const action = new DeleteCollection(col);
      const result = reducer(initialState, action);
      expect(result).toEqual(initialState);
    });
  });

  describe('DeleteCollectionSuccess action', () => {

    it('should return initial state if the element is not defined yet', () => {
      const action = new DeleteCollectionSuccess([]);
      const result = reducer(initialState, action);
      expect(result).toEqual(initialState);
    });

    it('should return the state with document containing the updated collection', () => {
      const col = new Collection().deserialize({ parent: path('test'), name: 'col' });
      const doc = new Document().deserialize({ id: 'test', collections: [col] });
      const initialStateWithDocument = { element: doc };
      const action = new DeleteCollectionSuccess([]);
      const result = reducer(initialStateWithDocument, action);
      expect((result.element as Document)).toEqual(new Document().deserialize({ id: 'test' }));
      expect((result.element as Document).collections).not.toContain(col);
    });

  });

  describe('DeleteCollectionFail action', () => {
    it('should return the previous state', () => {
      const col = new Collection().deserialize({ parent: path('test'), name: 'col' });
      const doc = new Document().deserialize({ id: 'test', collections: [col] });
      const state = { element: doc };
      const action = new DeleteCollectionFail(col);
      const result = reducer(state, action);
      expect(result).toEqual(state);
      expect(result.element).toEqual(doc);
    });
  });
});
