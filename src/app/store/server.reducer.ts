import { ServerActions, ServerActionTypes } from './server.actions';

export enum ServerStatus { UNKNOWN, UP, DOWN }

export interface State {
  host: string;
  port: number;
  apiKey: string;
  status: ServerStatus;
}

export const initialState: State = {
  host: null,
  port: -1,
  apiKey: null,
  status: ServerStatus.UNKNOWN,
};

export function reducer(state = initialState, action: ServerActions): State {
  switch (action.type) {
    case ServerActionTypes.CheckStatusUp: {
      return {
        ...state,
        status: ServerStatus.UP
      };
    }

    case ServerActionTypes.CheckStatusDown: {
      return {
        ...state,
        status: ServerStatus.DOWN
      };
    }

    default:
      return state;
  }
}
