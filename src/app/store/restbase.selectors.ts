import { createSelector } from '@ngrx/store';
import { State } from '.';

const selectRestbase = (state: State) => state.restbase;

export const selectElement = createSelector(
  selectRestbase,
  (state) => state.element
);
