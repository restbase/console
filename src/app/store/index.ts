import { ActionReducerMap, MetaReducer } from '@ngrx/store';
import { environment } from '../../environments/environment';
import * as fromServer from './server.reducer';
import * as fromRestbase from './restbase.reducer';

export interface State {
  server: fromServer.State;
  restbase: fromRestbase.State;
}

export const reducers: ActionReducerMap<State> = {
  server: fromServer.reducer,
  restbase: fromRestbase.reducer,
};


export const metaReducers: MetaReducer<State>[] = !environment.production ? [] : [];
