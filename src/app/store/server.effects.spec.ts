import { HttpClientModule } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { Actions } from '@ngrx/effects';
import { provideMockActions } from '@ngrx/effects/testing';
import { cold } from 'jasmine-marbles';
import { of, throwError } from 'rxjs';
import { Status } from '../model/server-health';
import { CheckStatus, CheckStatusDown, CheckStatusUp } from './server.actions';
import { ServerEffects } from './server.effects';

const createRestbaseStub = (response: any) => {
  const service = jasmine.createSpyObj('restbase', ['getHealth']);
  const isError = response instanceof Error;
  const serviceResponse = isError ? throwError(response) : of(response);
  service.getHealth.and.returnValue(serviceResponse);
  return service;
};

describe('ServerEffects', () => {

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientModule,
      ],
      providers: [
        ServerEffects,
        provideMockActions(() => of({}))
      ]
    });

  });

  it('should be created', () => {
    const effects = TestBed.get<ServerEffects>(ServerEffects);
    expect(effects).toBeTruthy();
  });

  it('should return UP action', () => {
    const source = cold('a', { a: new CheckStatus() });
    const service = createRestbaseStub({ status: Status.UP });
    const effects = new ServerEffects(new Actions(source), service);

    const expected = cold('a', { a: new CheckStatusUp() });
    expect(effects.checkStatus$).toBeObservable(expected);
  });


  it('should return DOWN action', () => {
    const source = cold('a', { a: new CheckStatus() });
    const service = createRestbaseStub({ status: Status.DOWN });
    const effects = new ServerEffects(new Actions(source), service);

    const expected = cold('a', { a: new CheckStatusDown() });
    expect(effects.checkStatus$).toBeObservable(expected);
  });

  it('should return DOWN action for missing status', () => {
    const source = cold('a', { a: new CheckStatus() });
    const service = createRestbaseStub({  });
    const effects = new ServerEffects(new Actions(source), service);

    const expected = cold('a', { a: new CheckStatusDown() });
    expect(effects.checkStatus$).toBeObservable(expected);
  });

  it('should return DOWN action for error', () => {
    const source = cold('a', { a: new CheckStatus() });
    const service = createRestbaseStub(new Error('unknown error'));
    const effects = new ServerEffects(new Actions(source), service);

    const expected = cold('a', { a: new CheckStatusDown() });
    expect(effects.checkStatus$).toBeObservable(expected);
  });

});
