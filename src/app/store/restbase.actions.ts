import { Action } from '@ngrx/store';
import { Collection } from '../model/collection';
import { Document } from '../model/document';
import { Element } from '../model/element';
import { Path } from '../model/path';

export enum RestbaseActionTypes {
  LoadElement = '[Restbase] Load Element',
  LoadElementSuccess = '[Restbase] Load Element Success',
  LoadElementFail = '[Restbase] Load Element Fail',
  AddDocument = '[Restbase] Add Document',
  AddDocumentSuccess = '[Restbase] Add Document Success',
  AddDocumentFail = '[Restbase] Add Document Fail',
  UpdateDocument = '[Restbase] Update Document',
  UpdateDocumentSuccess = '[Restbase] Update Document Success',
  UpdateDocumentFail = '[Restbase] Update Document Fail',
  DeleteDocument = '[Restbase] Delete Document',
  DeleteDocumentSuccess = '[Restbase] Delete Document Success',
  DeleteDocumentFail = '[Restbase] Delete Document Fail',
  AddCollection = '[Restbase] Add Collection',
  AddCollectionSuccess = '[Restbase] Add Collection Success',
  AddCollectionFail = '[Restbase] Add Collection Fail',
  UpdateCollection = '[Restbase] Update Collection',
  UpdateCollectionSuccess = '[Restbase] Update Collection Success',
  UpdateCollectionFail = '[Restbase] Update Collection Fail',
  DeleteCollection = '[Restbase] Delete Collection',
  DeleteCollectionSuccess = '[Restbase] Delete Collection Success',
  DeleteCollectionFail = '[Restbase] Delete Collection Fail',
}

export class LoadElement implements Action {
  readonly type = RestbaseActionTypes.LoadElement;
  readonly path: Path;
  constructor(path: Path) { this.path = path; }
}

export class LoadElementSuccess implements Action {
  readonly type = RestbaseActionTypes.LoadElementSuccess;

  readonly path: Path;
  readonly element: Element;

  constructor(
    path: Path,
    element: Element,
  ) {
    this.path = path;
    this.element = element;
  }
}

export class LoadElementFail implements Action {
  readonly type = RestbaseActionTypes.LoadElementFail;
  readonly path: Path;
  constructor(path: Path) { this.path = path; }
}

export class AddDocument implements Action {
  readonly type = RestbaseActionTypes.AddDocument;

  constructor(
    public readonly collection: Collection,
  ) { }
}

export class AddDocumentSuccess implements Action {
  readonly type = RestbaseActionTypes.AddDocumentSuccess;

  constructor(
    public readonly collection: Collection,
    public readonly document: Document,
  ) { }
}

export class AddDocumentFail implements Action {
  readonly type = RestbaseActionTypes.AddDocumentFail;
  constructor(
    public readonly collection: Collection,
  ) { }
}

export class UpdateDocument implements Action {
  readonly type = RestbaseActionTypes.UpdateDocument;

  constructor(
    public readonly path: Path,
    public readonly document: Document,
  ) { }
}

export class UpdateDocumentSuccess implements Action {
  readonly type = RestbaseActionTypes.UpdateDocumentSuccess;

  constructor(
    public readonly document: Document,
  ) { }
}

export class UpdateDocumentFail implements Action {
  readonly type = RestbaseActionTypes.UpdateDocumentFail;
  constructor(
    public readonly path: Path,
    public readonly document: Document,
  ) { }
}

export class DeleteDocument implements Action {
  readonly type = RestbaseActionTypes.DeleteDocument;

  constructor(
    public readonly document: Document,
  ) { }
}

export class DeleteDocumentSuccess implements Action {
  readonly type = RestbaseActionTypes.DeleteDocumentSuccess;

  constructor(
    public readonly document: Document,
  ) { }
}

export class DeleteDocumentFail implements Action {
  readonly type = RestbaseActionTypes.DeleteDocumentFail;
  constructor(
    public readonly document: Document,
  ) { }
}

export class AddCollection implements Action {
  readonly type = RestbaseActionTypes.AddCollection;

  constructor(
    public readonly document: Document,
    public readonly collectionName: string,
  ) { }
}

export class AddCollectionSuccess implements Action {
  readonly type = RestbaseActionTypes.AddCollectionSuccess;

  constructor(
    public readonly document: Document,
    public readonly collection: Collection,
  ) { }
}

export class AddCollectionFail implements Action {
  readonly type = RestbaseActionTypes.AddCollectionFail;
  constructor(
    public readonly document: Document,
    public readonly collectionName: string,
  ) { }
}

export class UpdateCollection implements Action {
  readonly type = RestbaseActionTypes.UpdateCollection;

  constructor(
    public readonly document: Document,
    public readonly collection: Collection,
    public readonly collectionName: string,
  ) { }
}

export class UpdateCollectionSuccess implements Action {
  readonly type = RestbaseActionTypes.UpdateCollectionSuccess;

  constructor(
    public readonly document: Document,
    public readonly collection: Collection,
    public readonly result: Collection,
  ) { }
}

export class UpdateCollectionFail implements Action {
  readonly type = RestbaseActionTypes.UpdateCollectionFail;
  constructor(
    public readonly document: Document,
    public readonly collection: Collection,
    public readonly collectionName: string,
  ) { }
}

export class DeleteCollection implements Action {
  readonly type = RestbaseActionTypes.DeleteCollection;

  constructor(
    public readonly collection: Collection,
  ) { }
}

export class DeleteCollectionSuccess implements Action {
  readonly type = RestbaseActionTypes.DeleteCollectionSuccess;

  constructor(
    public readonly collections: Collection[],
  ) { }
}

export class DeleteCollectionFail implements Action {
  readonly type = RestbaseActionTypes.DeleteCollectionFail;
  constructor(
    public readonly collection: Collection,
  ) { }
}

export type RestbaseActions
  = LoadElement
  | LoadElementSuccess
  | LoadElementFail
  | AddDocument
  | AddDocumentSuccess
  | AddDocumentFail
  | UpdateDocument
  | UpdateDocumentSuccess
  | UpdateDocumentFail
  | DeleteDocument
  | DeleteDocumentSuccess
  | DeleteDocumentFail
  | AddCollection
  | AddCollectionSuccess
  | AddCollectionFail
  | UpdateCollection
  | UpdateCollectionSuccess
  | UpdateCollectionFail
  | DeleteCollection
  | DeleteCollectionSuccess
  | DeleteCollectionFail
  ;
