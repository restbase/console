import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';
import { RestbaseService } from '../restbase/restbase.service';
import {
  RestbaseActionTypes,
  LoadElement, LoadElementFail, LoadElementSuccess,
  AddDocument, AddDocumentSuccess, AddDocumentFail,
  UpdateDocument, UpdateDocumentSuccess, UpdateDocumentFail,
  DeleteDocument, DeleteDocumentSuccess, DeleteDocumentFail,
  AddCollection, AddCollectionSuccess, AddCollectionFail,
  UpdateCollection, UpdateCollectionSuccess, UpdateCollectionFail,
  DeleteCollection, DeleteCollectionSuccess, DeleteCollectionFail,
} from './restbase.actions';

@Injectable()
export class RestbaseEffects {

  @Effect()
  loadElement$ = this.actions$.pipe(
    ofType<LoadElement>(RestbaseActionTypes.LoadElement),
    switchMap((action) => this.restbase.getElement(action.path)
      .pipe(
        map((result) => new LoadElementSuccess(action.path, result)),
        catchError(() => of(new LoadElementFail(action.path)))
      )),
  );

  @Effect()
  addDocument$ = this.actions$.pipe(
    ofType<AddDocument>(RestbaseActionTypes.AddDocument),
    switchMap((action) => this.restbase.addDocument(action.collection)
      .pipe(
        map((result) => new AddDocumentSuccess(action.collection, result)),
        catchError(() => of(new AddDocumentFail(action.collection)))
      )),
  );

  @Effect()
  updateDocument$ = this.actions$.pipe(
    ofType<UpdateDocument>(RestbaseActionTypes.UpdateDocument),
    switchMap((action) => this.restbase.updateDocument(action.path, action.document)
      .pipe(
        map((result) => new UpdateDocumentSuccess(result)),
        catchError(() => of(new UpdateDocumentFail(action.path, action.document)))
      )),
  );

  @Effect()
  deleteDocument$ = this.actions$.pipe(
    ofType<DeleteDocument>(RestbaseActionTypes.DeleteDocument),
    switchMap((action) => this.restbase.deleteDocument(action.document)
      .pipe(
        map((result) => new DeleteDocumentSuccess(result)),
        catchError(() => of(new DeleteDocumentFail(action.document)))
      )),
  );

  @Effect()
  addCollection$ = this.actions$.pipe(
    ofType<AddCollection>(RestbaseActionTypes.AddCollection),
    switchMap((action) => this.restbase.addCollection(action.document, action.collectionName)
      .pipe(
        map((result) => new AddCollectionSuccess(action.document, result)),
        catchError(() => of(new AddCollectionFail(action.document, action.collectionName)))
      )),
  );

  @Effect()
  updateCollection$ = this.actions$.pipe(
    ofType<UpdateCollection>(RestbaseActionTypes.UpdateCollection),
    switchMap((action) => this.restbase.updateCollection(action.document, action.collection, action.collectionName)
      .pipe(
        map((result) => new UpdateCollectionSuccess(action.document, action.collection, result)),
        catchError(() => of(new UpdateCollectionFail(action.document, action.collection, action.collectionName))),
      )),
  );

  @Effect()
  deleteCollection$ = this.actions$.pipe(
    ofType<DeleteCollection>(RestbaseActionTypes.DeleteCollection),
    switchMap((action) => this.restbase.deleteCollection(action.collection)
      .pipe(
        map((result) => new DeleteCollectionSuccess(result)),
        catchError(() => of(new DeleteCollectionFail(action.collection))),
      )),
  );

  constructor(
    private actions$: Actions,
    private restbase: RestbaseService,
  ) { }

}
