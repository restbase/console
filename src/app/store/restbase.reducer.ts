import { Element } from '../model/element';
import { Document } from '../model/document';
import { RestbaseActions, RestbaseActionTypes } from './restbase.actions';
import { Collection } from '../model/collection';

export interface State {
  element?: Element;
}

export const initialState: State = {
};

export function reducer(state = initialState, action: RestbaseActions): State {

  switch (action.type) {

    case RestbaseActionTypes.LoadElement:
    case RestbaseActionTypes.AddDocument:
    case RestbaseActionTypes.UpdateDocument:
    case RestbaseActionTypes.DeleteDocument:
    case RestbaseActionTypes.AddCollection:
    case RestbaseActionTypes.DeleteCollection:
    case RestbaseActionTypes.UpdateCollection: {
      return {
        ...state,
      };
    }

    case RestbaseActionTypes.LoadElementSuccess: {
      return {
        ...state,
        element: action.element
      };
    }

    case RestbaseActionTypes.AddDocumentSuccess: {
      if (!state.element || (state.element && state.element.isCollection())) {
        const col = action.collection;
        const doc = action.document;
        if (!col.documents) { col.documents = []; }
        col.documents = [
          ...col.documents,
          doc
        ];
        return {
          ...state,
          element: col
        };
      }
      return {
        ...state,
      };
    }

    case RestbaseActionTypes.UpdateDocumentSuccess: {
      if (!state.element || (state.element && state.element.isDocument())) {
        return {
          ...state,
          element: action.document
        };
      }
      return {
        ...state,
      };
    }

    case RestbaseActionTypes.DeleteDocumentSuccess: {
      if (state.element && state.element.isCollection()) {
        const col = state.element as Collection;
        const doc = action.document;
        const index = col.documents.findIndex((d => d.id === doc.id));
        if (index > -1) {
          col.documents.splice(index, 1);
          return {
            ...state,
            element: col
          };
        }
      }
      return {
        ...state,
      };
    }

    case RestbaseActionTypes.AddCollectionSuccess: {
      if (!state.element || (state.element && state.element.isDocument())) {
        const col = action.collection;
        const collections = [...action.document.collections, col];
        const element: Document = new Document().deserialize({
          ...action.document,
          collections
        });
        return {
          ...state,
          element
        };
      }
      return {
        ...state,
      };
    }

    case RestbaseActionTypes.UpdateCollectionSuccess: {
      if (!state.element || (state.element && state.element.isDocument())) {
        const doc = action.document;
        const oldCollection = action.collection;
        const newCollection = action.result;
        const index = doc.collections.findIndex((col => col.id === oldCollection.id));
        if (index > -1) {
          doc.collections[index] = newCollection;
          return {
            ...state,
            element: doc
          };
        }
      }
      return {
        ...state,
      };
    }

    case RestbaseActionTypes.DeleteCollectionSuccess: {
      if (state.element && state.element.isDocument()) {
        const collections = action.collections;
        const doc = new Document().deserialize({
          ...state.element,
          collections
        });
        return {
          ...state,
          element: doc
        };
      }
      return {
        ...state,
      };
    }

    case RestbaseActionTypes.LoadElementFail:
    case RestbaseActionTypes.AddDocumentFail:
    case RestbaseActionTypes.UpdateDocumentFail:
    case RestbaseActionTypes.DeleteDocumentFail:
    case RestbaseActionTypes.AddCollectionFail:
    case RestbaseActionTypes.DeleteCollectionFail:
    case RestbaseActionTypes.UpdateCollectionFail: {
      return {
        ...state,
      };
    }

    default:
      return state;
  }
}
