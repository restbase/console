import { createSelector } from '@ngrx/store';
import { State } from '.';

const selectServer = (state: State) => state.server;

export const selectServerStatus = createSelector(
  selectServer,
  (state) => state.status
);
