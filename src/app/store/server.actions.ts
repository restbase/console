import { Action } from '@ngrx/store';

export enum ServerActionTypes {
  CheckStatus = '[Server] Check Status',
  CheckStatusUp = '[Server] Check Status UP',
  CheckStatusDown = '[Server] Check Status DOWN',
}

export class CheckStatus implements Action {
  readonly type = ServerActionTypes.CheckStatus;
}

export class CheckStatusUp implements Action {
  readonly type = ServerActionTypes.CheckStatusUp;
}

export class CheckStatusDown implements Action {
  readonly type = ServerActionTypes.CheckStatusDown;
}

export type ServerActions
  = CheckStatus
  | CheckStatusUp
  | CheckStatusDown
  ;
