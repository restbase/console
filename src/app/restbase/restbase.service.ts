import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { Collection } from '../model/collection';
import { Document } from '../model/document';
import { Element } from '../model/element';
import { Path } from '../model/path';
import { ServerHealth } from '../model/server-health';
import { map, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class RestbaseService {
  static readonly SERVER = 'http://localhost:8080';

  constructor(
    private http: HttpClient,
  ) { }

  getRoot(): Observable<Document> {
    return this.http.get<Document>(`${RestbaseService.SERVER}/api`);
  }

  getElement(path: Path): Observable<Element> {
    const p = path.joined();
    if (path.isDocument()) {
      return this.http.get<Document>(`${RestbaseService.SERVER}/api/${p}`)
        .pipe(
          map((data) => new Document().deserialize(data)),
          catchError((err) => throwError(`Document not found: ${err}`))
        );
    }
    return this.http.get<Collection>(`${RestbaseService.SERVER}/api/${p}`)
      .pipe(
        map(data => new Collection().deserialize(data)),
        catchError((err) => throwError(`Collection not found: ${err}`))
      );
  }

  addDocument(collection: Collection): Observable<Document> {
    const path = collection.path();
    const p = path.joined();
    return this.http.post<Document>(`${RestbaseService.SERVER}/api/${p}`, collection)
      .pipe(
        map(data => new Document().deserialize(data)),
        catchError((err) => throwError(`Collection not found: ${err}`))
      );
  }

  updateDocument(path: Path, document: Document): Observable<Document> {
    const p = path.joined();
    return this.http.put<Document>(`${RestbaseService.SERVER}/api/${p}`, document)
      .pipe(
        map(data => new Document().deserialize(data)),
        catchError((err) => throwError(`Document not found: ${err}`))
      );
  }

  deleteDocument(document: Document): Observable<Document> {
    const path = document.path().joined();
    return this.http.delete<Document>(`${RestbaseService.SERVER}/api/${path}`)
      .pipe(
        map(data => new Document().deserialize(data)),
        catchError((err) => throwError(`Document not found: ${err}`))
      );
  }

  addCollection(document: Document, collectionName: string): Observable<Collection> {
    const path = document.path();
    const p = path.joined();
    const collection = new Collection().deserialize({ parent: path, name: collectionName });
    return this.http.post<Collection>(`${RestbaseService.SERVER}/api/${p}`, collection)
      .pipe(
        map(data => new Collection().deserialize(data)),
        catchError((err) => throwError(`Collection not found: ${err}`))
      );
  }

  updateCollection(document: Document, collection: Collection, collectionName: string): Observable<Collection> {
    const result = new Collection().deserialize({ ...collection, name: collectionName });
    return of(result);
  }

  deleteCollection(collection: Collection): Observable<Collection[]> {
    const path = collection.path().joined();
    return this.http.delete<Collection[]>(`${RestbaseService.SERVER}/api/${path}`)
      .pipe(
        map(data => data.map(c => new Collection().deserialize(c))),
        catchError((err) => throwError(`Collection not found: ${err}`))
      );
  }

  getHealth() {
    return this.http.get<ServerHealth>(`${RestbaseService.SERVER}/actuator/health`);
  }

}
