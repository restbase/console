import { HttpClientModule } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { getTestBed, TestBed } from '@angular/core/testing';
import { Collection } from '../model/collection';
import { Document } from '../model/document';
import { path, ROOT } from '../model/path';
import { ServerHealth, Status } from '../model/server-health';
import { RestbaseService } from './restbase.service';


describe('RestbaseService', () => {
  let httpMock: HttpTestingController;
  let service: RestbaseService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientModule,
        HttpClientTestingModule
      ],
      providers: [
        RestbaseService,
      ],
    });
    const injector = getTestBed();
    service = TestBed.get(RestbaseService);
    httpMock = injector.get(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('getRoot() should return ROOT', () => {
    const response = Document.ROOT;
    service.getRoot().subscribe(result => expect(result).toEqual(response));

    const request = httpMock.expectOne(`${RestbaseService.SERVER}/api`);
    expect(request.cancelled).toBeFalsy();
    expect(request.request.responseType).toEqual('json');
    request.flush(response);
  });

  it('getElement() should return a collection', () => {
    const response = new Collection().deserialize({ name: 'a', parent: ROOT });
    service.getElement(path('a')).subscribe(element => expect(element).toEqual(response));

    const request = httpMock.expectOne(`${RestbaseService.SERVER}/api/a`);
    expect(request.cancelled).toBeFalsy();
    expect(request.request.responseType).toEqual('json');
    request.flush(response);
  });

  it('getElement() should return a document', () => {
    const someDoc = { collection: 'a', id: 'b' };
    service.getElement(path('a/b')).subscribe(element => {
      expect(element).toEqual(jasmine.objectContaining(someDoc));
    });

    const request = httpMock.expectOne(`${RestbaseService.SERVER}/api/a/b`);
    expect(request.cancelled).toBeFalsy();
    expect(request.request.responseType).toEqual('json');
    request.flush(someDoc);
  });

  describe('.addDocument()', () => {

    it('should return a document', () => {
      const collection = new Collection().deserialize({ parent: ROOT, name: 'test' });
      const response: Partial<Document> = { collection: 'test', id: 'b' };
      service.addDocument(collection).subscribe(element => {
        return expect(element).toEqual(jasmine.objectContaining(response));
      });

      const request = httpMock.expectOne(`${RestbaseService.SERVER}/api/test`);
      expect(request.cancelled).toBeFalsy();
      expect(request.request.responseType).toEqual('json');
      request.flush(response);
    });

  });

  describe('.getHealth()', () => {

    it('should return an Observable<ServerHealth>', () => {
      const status: ServerHealth = {
        status: Status.UP
      };

      service.getHealth().subscribe(health => {
        expect(health).toBeDefined();
        expect(health).toEqual(status);
      });

      const req = httpMock.expectOne(`${RestbaseService.SERVER}/actuator/health`);
      expect(req.request.method).toBe('GET');
      req.flush(status);
    });

  });

  afterEach(() => {
    httpMock.verify();
  });

});
